#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import os
import sys
import platform
import PyInstaller.__main__
from PIL import Image

project = 'TimeManager'

dir_dist = 'dist_bin'
dir_build = 'build_bin'

dir_resource = 'pkg/time_manager/_resource'


def assert_working_dir():
    root_dir = os.path.dirname(os.path.realpath(__file__))
    if os.getcwd() != root_dir:
        sys.stderr.write("[ERROR]: Please change working directory to: {}\n".format(root_dir))
        sys.exit(1)


def mkdir(dir_path):
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)


def get_version():
    with open('pkg/time_manager/_resource/version.txt') as f:
        return f.read().strip()


def add_icon(params):
    file_image = os.path.join(dir_resource, 'image/appicon.png')
    name_icon = 'appicon.ico'
    file_icon = os.path.join(dir_build, name_icon)

    img = Image.open(file_image)
    mkdir(dir_build)
    icon_sizes = [(16, 16), (24, 24), (32, 32), (48, 48), (64, 64), (128, 128), (255, 255)]
    img.save(file_icon, sizes=icon_sizes)

    params.append('--icon={}'.format(name_icon))


def add_version_rc(params):
    version_str = get_version()
    version_rc = '''VSVersionInfo(
        ffi=FixedFileInfo(
            filevers=({0},0),
            prodvers=({0},0),
            mask=0x3f,
            flags=0x0,
            OS=0x40004,
            fileType=0x1,
            subtype=0x0,
            date=(0,0)
        ),
        kids=[
            StringFileInfo([
                StringTable(u'040904B0', [
                        StringStruct(u'FileVersion', u'{1}'),
                        StringStruct(u'ProductVersion', u'{1}'),
                        StringStruct(u'ProductName', u'{2}'),
                        StringStruct(u'InternalName', u'{2}'),
                        StringStruct(u'OriginalFilename', u'{2}.exe'),
                        StringStruct(u'LegalCopyright', u'Copyright by P.J. Grochowski'),
                        StringStruct(u'FileDescription', u'')
                ])
            ]),
            VarFileInfo([VarStruct(u'Translation', [1033, 1200])])
        ]
    )
'''.format(version_str.replace('.', ','), version_str, project)

    name_version_rc = 'version_rc.txt'
    file_version_rc = os.path.join(dir_build, name_version_rc)
    mkdir(dir_build)
    with open(file_version_rc, 'w') as f:
        f.write(version_rc)

    params.append('--version-file={}'.format(name_version_rc))


def setup():
    assert_working_dir()
    
    params = [
        '--distpath={}'.format(dir_dist),
        '--workpath={}'.format(dir_build),
        '--specpath={}'.format(dir_build),
        '--onefile',
        '--windowed',
        '--name=%s' % project,
    ]

    if platform.system() == 'Windows':
        add_icon(params)
        add_version_rc(params)

    script = os.path.join('script', project)
    params.append(script)

    PyInstaller.__main__.run(params)


if __name__ == "__main__":
    setup()
