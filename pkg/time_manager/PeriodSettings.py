#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski


class PeriodSettings:

    DEFAULT_PERIOD_UNIT = 'Periods'

    def __init__(self):
        self._periodCount = 0
        self._periodUnit = self.DEFAULT_PERIOD_UNIT

    @property
    def periodCount(self) -> int:
        return self._periodCount

    @periodCount.setter
    def periodCount(self, value: int) -> None:
        self._periodCount = value

    @property
    def periodUnit(self) -> str:
        return self._periodUnit

    @periodUnit.setter
    def periodUnit(self, value: str) -> None:
        if not value:
            value = self.DEFAULT_PERIOD_UNIT
        self._periodUnit = str(value)
