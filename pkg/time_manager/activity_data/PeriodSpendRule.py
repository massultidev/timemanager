#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import functools
from typing import *


@functools.total_ordering
class PeriodSpendRule:

    MAX_POINTS_PER_RULE = 100

    def __init__(self, periods: Optional[int] = 0, valueToGain: Optional[int] = 0):
        self._periods = periods
        self._valueToGain = valueToGain

    def __eq__(self, other: Any) -> bool:
        def makeTuple(rule: PeriodSpendRule):
            return (
                rule._periods,
                rule._valueToGain
            )
        return makeTuple(other) == makeTuple(self)

    def __lt__(self, other: Any) -> bool:
        def getComparisonDeterminant(obj: PeriodSpendRule):
            return obj._periods
        return getComparisonDeterminant(other) > getComparisonDeterminant(self)

    @property
    def periods(self) -> int:
        return self._periods

    @periods.setter
    def periods(self, value: int) -> None:
        self._periods = value

    @property
    def valueToGain(self) -> int:
        return self._valueToGain

    @valueToGain.setter
    def valueToGain(self, value: int) -> None:
        self._valueToGain = value
