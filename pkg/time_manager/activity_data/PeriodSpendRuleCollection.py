#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from typing import *
from time_manager.activity_data.PeriodSpendRule import PeriodSpendRule


class PeriodSpendRuleCollection:

    def __init__(self):
        self._rules = None

        self.clear()

    def __eq__(self, other: Any) -> bool:
        def getComparisonDeterminant(obj: PeriodSpendRuleCollection):
            return obj._rules
        return getComparisonDeterminant(other) == getComparisonDeterminant(self)

    def __len__(self) -> int:
        return len(self._rules)

    def clear(self) -> None:
        self._rules = {}

    def keys(self) -> Iterator[int]:
        return self._rules.keys()

    def values(self) -> Iterator[PeriodSpendRule]:
        return self._rules.values()

    def has(self, periods: int) -> bool:
        return periods in self.keys()

    def get(self, periods: int) -> PeriodSpendRule:
        if not self.has(periods):
            raise RuntimeError('Cannot get! Spend rule for "{}" periods does not exists in collection!'.format(periods))
        return self._rules[periods]

    def add(self, rule: PeriodSpendRule) -> None:
        if self.has(rule.periods):
            raise RuntimeError('Cannot add! Spend rule for "{}" periods already exists in collection!'.format(rule.periods))
        self._rules[rule.periods] = rule

    def update(self, rule: PeriodSpendRule) -> None:
        if not self.has(rule.periods):
            raise RuntimeError('Cannot update! Spend rule for "{}" periods does not exists in collection!'.format(rule.periods))
        self._rules[rule.periods] = rule

    def remove(self, periods: int) -> None:
        if not self.has(periods):
            raise RuntimeError('Cannot remove! Spend rule for "{}" periods does not exists in collection!'.format(periods))
        self._rules.pop(periods)
