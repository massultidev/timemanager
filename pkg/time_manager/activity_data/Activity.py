#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from typing import *
from time_manager.activity_data.PeriodSpendRuleCollection import PeriodSpendRuleCollection


class Activity:

    def __init__(self, name: Optional[str] = '', isRequired: Optional[bool] = False):
        self._name = name
        self._isRequired = isRequired
        self._periodSpendRules = PeriodSpendRuleCollection()

    def __eq__(self, other: Any) -> bool:
        def makeTuple(activity: Activity):
            return (
                activity._name,
                activity._isRequired,
                activity._periodSpendRules
            )
        return makeTuple(other) == makeTuple(self)

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, value: str) -> None:
        self._name = value

    @property
    def isRequired(self) -> bool:
        return self._isRequired

    @isRequired.setter
    def isRequired(self, value: bool) -> None:
        self._isRequired = value

    @property
    def periodSpendRules(self) -> PeriodSpendRuleCollection:
        return self._periodSpendRules
