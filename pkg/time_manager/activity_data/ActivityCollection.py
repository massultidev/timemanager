#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from typing import *
from collections import OrderedDict
from time_manager.activity_data.Activity import Activity


class ActivityCollection:

    def __init__(self):
        self._activities = None

        self.clear()

    def __len__(self) -> int:
        return len(self._activities)

    def clear(self) -> None:
        self._activities = OrderedDict()

    def keys(self) -> Iterator[str]:
        return self._activities.keys()

    def values(self) -> Iterator[Activity]:
        return self._activities.values()

    def has(self, activityName: str) -> bool:
        return activityName in self.keys()

    def get(self, activityName: str) -> Activity:
        if not self.has(activityName):
            raise RuntimeError('Cannot get! Activity by name "{}" does not exists in collection!'.format(activityName))
        return self._activities[activityName]

    def add(self, activity: Activity) -> None:
        if self.has(activity.name):
            raise RuntimeError('Cannot add! Activity by name "{}" already exists in collection!'.format(activity.name))
        self._activities[activity.name] = activity

    def update(self, activity: Activity) -> None:
        if not self.has(activity.name):
            raise RuntimeError('Cannot update! Activity by name "{}" does not exists in collection!'.format(activity.name))
        self._activities[activity.name] = activity

    def remove(self, activityName: str) -> None:
        if not self.has(activityName):
            raise RuntimeError('Cannot remove! Activity by name "{}" does not exists in collection!'.format(activityName))
        self._activities.pop(activityName)

    def moveUp(self, activityName: str) -> None:
        self._move(activityName, (-1))

    def moveDown(self, activityName: str) -> None:
        self._move(activityName, (+1))

    def _move(self, activityName: str, modifier: int) -> None:
        if not self.has(activityName):
            raise RuntimeError('Cannot move! Activity by name "{}" does not exists in collection!'.format(activityName))

        keyOrder = self._getNewKeyOrder(activityName, modifier)
        self._activities = OrderedDict((k, self._activities[k]) for k in keyOrder)

    def _getNewKeyOrder(self, activityName: str, modifier: int) -> List[str]:
        activityNames = list(self.keys())
        activityIndex = activityNames.index(activityName)
        activityNames.pop(activityIndex)

        activityIndex += modifier
        activityIndex = activityIndex % len(self)

        activityNames.insert(activityIndex, activityName)

        return activityNames
