#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from typing import *
from gdev_utility.observer.EventBase import EventBase
from time_manager.ViewStates import ActivityViewState, PeriodSpendRuleViewState, PeriodSettingsViewState, \
    ProcessDataViewState


class PeriodSettingsStateEvent(EventBase):

    def __init__(self, eventId: PeriodSettingsViewState):
        super().__init__(eventId=eventId)


class ActivityStateEvent(EventBase):

    def __init__(self, eventId: ActivityViewState, activityName: Optional[str] = None):
        super().__init__(eventId=eventId)

        self._activityName = activityName

    @property
    def activityName(self) -> str:
        return self._activityName


class PeriodSpendRuleStateEvent(EventBase):

    def __init__(self, eventId: PeriodSpendRuleViewState, periods: Optional[int] = None):
        super().__init__(eventId=eventId)

        self._periods = periods

    @property
    def periods(self) -> int:
        return self._periods


class ProcessDataStateEvent(EventBase):

    def __init__(self, eventId: ProcessDataViewState, result: Optional[Any] = None):
        super().__init__(eventId=eventId)

        self._result = result

    @property
    def result(self) -> Any:
        return self._result
