#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from enum import Enum, auto


class PeriodSettingsViewState(Enum):
    Edit = auto()
    Apply = auto()
    Dismiss = auto()


class ActivityViewState(Enum):
    Display = auto()
    Add = auto()
    Edit = auto()
    Duplicate = auto()
    Apply = auto()
    Dismiss = auto()


class PeriodSpendRuleViewState(Enum):
    Add = auto()
    Edit = auto()
    Apply = auto()
    Dismiss = auto()


class ProcessDataViewState(Enum):
    Process = auto()
    Display = auto()
    Dismiss = auto()
    Error = auto()
