#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski


class ActivityData:
    """This class is a single activity representation used to provide raw numeric data.

    Arguments:
        rules (dict): Dictionary of periods_amount-value numeric rules.
    """

    def __init__(self, rules):
        for key in rules.keys():
            if not isinstance(key, int):
                raise Exception(
                    "ActivityData constructor expects dictionary consisting only of numeric keys!")
        for value in rules.values():
            if not isinstance(value, int):
                raise Exception(
                    "ActivityData constructor expects dictionary consisting only of numeric values!")
        self.__rules = rules

    def getValueForPeriods(self, periods):
        """Gets value for periods_amount according to activity rules.

        Arguments:
            periods (int): Periods amount.

        Return:
            int: Value for given periods amount.
        """
        maxPeriod = max(self.__rules.keys())
        minPeriod = min(self.__rules.keys())
        if periods > maxPeriod:
            return self.__rules[maxPeriod]
        elif periods < minPeriod:
            return 0
        if periods not in self.__rules:
            return self.__rules[min(
                self.__rules.keys(), key=lambda x:abs(x - periods))]
        return self.__rules[periods]
