#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import inspect
import copy

from time_manager.algorithm.ActivityData import ActivityData


class ActivityDataProcessor:
    """This class is a processor for dynamic programming discrete knapsack problem solving algorithm.

    Single instance is not reusable for another set of data.

    Arguments:
        activities (list): List of instances of ActivityData class. (Order matters!)
        periods (int): Amount of all periods to distribute.
    """

    def __init__(self, activities, periods):
        for activity in activities:
            if not isinstance(activity, ActivityData):
                raise Exception(
                    "Parameter '%s' takes list 'ActivityData' instances only!"
                    % inspect.currentframe().f_code.co_varnames[1]
                )
        if not isinstance(periods, int):
            raise Exception(
                "Parameter '%s' takes 'int' value only!"
                % inspect.currentframe().f_code.co_varnames[2]
            )
        self._activities = activities
        self._periods = periods
        self._results = None

    def run(self):
        """Runs dynamic programming algorithm solving knapsack problem.

        May take time, so it is advised to run it in a separate thread.
        """
        steps = [{periods: ([0], 0) for periods in range(self._periods + 1)}]
        for activity in reversed(self._activities):
            step = dict()
            for periods in range(self._periods + 1):
                preStep = dict()
                for periodsVariant in range(periods + 1):
                    preStep[periodsVariant] = activity.getValueForPeriods(
                        periodsVariant) + steps[-1][periods - periodsVariant][1]
                maxValue = max(preStep.values())
                maxSpares = []
                for item in list(preStep.items()):
                    if item[1] == maxValue:
                        maxSpares.append(item[0])
                step[periods] = (maxSpares, maxValue)
            steps.append(step)
        steps.pop(0)

        paths = [(self._periods - item, [item])
                 for item in steps[-1][self._periods][0]]
        for step in reversed(steps[:-1]):
            pathsNew = []
            for path in paths:
                possibs = step[path[0]][0]
                for possib in possibs:
                    pth = copy.deepcopy(path[1])
                    pth.append(possib)
                    pathNew = (path[0] - possib, pth)
                    if not str(pathNew) in [str(pathStr) for pathStr in paths]:
                        pathsNew.append(pathNew)
            paths = pathsNew

        self._results = paths

    def getResult(self):
        """Returns result of processing or None if called before run() method.

        Return:
            list: List of tuples, where every tuple has pattern:\n
            (remaining_periods_number, decisions)\n
            Where 'decisions' is a list of periods spent on activities in order activities were provided in constructor.
        """
        return self._results
