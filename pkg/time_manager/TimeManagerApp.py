#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import platform
import wx
import re
import sys
import logging
import logging.handlers
import time_manager
import os
from gdev_gui_wx.App import App
from gdev_gui_wx.SplashScreen import SplashScreen
from gdev_utility.Loggable import Loggable
from gdev_utility.ResourceProvider import ResourceProvider
from time_manager.AppInfo import AppInfo
from time_manager.PeriodSettings import PeriodSettings
from time_manager.StateObserver import StateObserver
from time_manager.activity_data.ActivityCollection import ActivityCollection
from time_manager.gui.window.FrameModel import FrameModel
from time_manager.gui.window.FrameViewModel import FrameViewModel


class TimeManagerApp(App, Loggable):

    def __init__(self):
        super().__init__()

        self._resourceProvider = ResourceProvider(package=time_manager)
        self._appInfo = AppInfo(resourceProvider=self._resourceProvider)
        self._periodSettings = PeriodSettings()
        self._activityCollection = ActivityCollection()
        self._stateObserver = StateObserver()

        frameModel = FrameModel(
            resourceProvider=self._resourceProvider,
            appInfo=self._appInfo,
            periodSettings=self._periodSettings,
            activityCollection=self._activityCollection,
            stateObserver=self._stateObserver
        )
        self._frame = FrameViewModel(model=frameModel)

        self._initLog()

    def run(self) -> None:
        self.log.info('Log file at: "{}"'.format(self._getLogFilePath()))
        self.log.info('Operating system: {} {}'.format(platform.system(), platform.release()))
        self.log.info('Interpretter version: {}'.format(re.sub('[\r\n]', '', sys.version)))
        self.log.info('wxPython version: {}'.format(wx.version()))
        self.log.info('Application: {} {}'.format(self._appInfo.name, self._appInfo.version))

        try:
            SplashScreen(self._frame.view, self._resourceProvider, 'image/splash.png')

            self.MainLoop()

        except Exception as e:
            self.log.critical(e)
            wx.MessageBox(
                caption='Error',
                message='Application has faced a critical error and will be closed!\n'
                        '(For details refer to log at: "{}")'
                        .format(self._getLogFilePath()),
                style=wx.OK | wx.ICON_ERROR | wx.CENTER
            )

    def _initLog(self) -> None:
        logFormatter = logging.Formatter(
            fmt='[%(asctime)s][%(threadName)s][%(levelname)-8.8s][%(name)s]: %(message)s',
            datefmt='%d/%m/%Y %I:%M:%S %p'
        )
        rootLogger = logging.getLogger()
        rootLogger.setLevel(logging.INFO)

        fileHandler = logging.handlers.RotatingFileHandler(
            filename=self._getLogFilePath(),
            maxBytes=(1048576*5),
            backupCount=7
        )
        fileHandler.setFormatter(logFormatter)
        rootLogger.addHandler(fileHandler)

        consoleHandler = logging.StreamHandler()
        consoleHandler.setFormatter(logFormatter)
        rootLogger.addHandler(consoleHandler)

    def _getLogFilePath(self) -> str:
        homeDir = os.path.expanduser('~')
        appDir = os.path.join(homeDir, '.py_app_time_manager')
        if not os.path.exists(appDir):
            os.makedirs(appDir)
        logFile = os.path.join(appDir, 'log.txt')
        return logFile
