#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from typing import *
from gdev_utility.observer.EventObserver import EventObserver
from gdev_utility.observer.Subscription import Subscription
from time_manager.StateEvents import ActivityStateEvent, PeriodSpendRuleStateEvent, PeriodSettingsStateEvent, \
    ProcessDataStateEvent
from time_manager.ViewStates import ActivityViewState, PeriodSpendRuleViewState, PeriodSettingsViewState, \
    ProcessDataViewState


class StateObserver:

    def __init__(self):
        self._periodSettingsViewStateObserver = EventObserver(PeriodSettingsViewState)
        self._activityViewStateObserver = EventObserver(ActivityViewState)
        self._periodSpendRuleViewStateObserver = EventObserver(PeriodSpendRuleViewState)
        self._processDataViewStateObserver = EventObserver(ProcessDataViewState)

    def subscribePeriodSettingsStates(
        self,
        states: List[PeriodSettingsViewState],
        callback: Callable[[PeriodSettingsStateEvent], None]
    ) -> Subscription:
        return self._periodSettingsViewStateObserver.subscribeEvents(
            eventIds=states,
            callback=callback
        )

    def subscribeActivityStates(
        self,
        states: List[ActivityViewState],
        callback: Callable[[ActivityStateEvent], None]
    ) -> Subscription:
        return self._activityViewStateObserver.subscribeEvents(
            eventIds=states,
            callback=callback
        )

    def subscribePeriodSpendRuleStates(
        self,
        states: List[PeriodSpendRuleViewState],
        callback: Callable[[PeriodSpendRuleViewState], None]
    ) -> Subscription:
        return self._periodSpendRuleViewStateObserver.subscribeEvents(
            eventIds=states,
            callback=callback
        )

    def subscribeProcessDataStates(
        self,
        states: ProcessDataViewState,
        callback: Callable[[ProcessDataStateEvent], None]
    ) -> Subscription:
        return self._processDataViewStateObserver.subscribeEvents(
            eventIds=states,
            callback=callback
        )

    def notifyPeriodSettingsEdit(self) -> None:
        self._notifyPeriodSettings(PeriodSettingsViewState.Edit)

    def notifyPeriodSettingsApply(self) -> None:
        self._notifyPeriodSettings(PeriodSettingsViewState.Apply)

    def notifyPeriodSettingsDismiss(self) -> None:
        self._notifyPeriodSettings(PeriodSettingsViewState.Dismiss)

    def notifyActivityDisplay(self, activityName: str) -> None:
        self._notifyActivity(ActivityViewState.Display, activityName)

    def notifyActivityAdd(self) -> None:
        self._notifyActivity(ActivityViewState.Add)

    def notifyActivityEdit(self, activityName: str) -> None:
        self._notifyActivity(ActivityViewState.Edit, activityName)

    def notifyActivityDuplicate(self, activityName: str) -> None:
        self._notifyActivity(ActivityViewState.Duplicate, activityName)

    def notifyActivityApply(self, activityName: str) -> None:
        self._notifyActivity(ActivityViewState.Apply, activityName)

    def notifyActivityDismiss(self) -> None:
        self._notifyActivity(ActivityViewState.Dismiss)

    def notifyPeriodSpendRuleAdd(self) -> None:
        self._notifyPeriodSpendRule(PeriodSpendRuleViewState.Add)

    def notifyPeriodSpendRuleEdit(self, periods: int) -> None:
        self._notifyPeriodSpendRule(PeriodSpendRuleViewState.Edit, periods)

    def notifyPeriodSpendRuleApply(self, periods: int) -> None:
        self._notifyPeriodSpendRule(PeriodSpendRuleViewState.Apply, periods)

    def notifyPeriodSpendRuleDismiss(self) -> None:
        self._notifyPeriodSpendRule(PeriodSpendRuleViewState.Dismiss)

    def notifyProcessDataProcess(self) -> None:
        self._notifyProcessData(ProcessDataViewState.Process)

    def notifyProcessDataDisplay(self, result: Any) -> None:
        self._notifyProcessData(ProcessDataViewState.Display, result)

    def notifyProcessDataDismiss(self) -> None:
        self._notifyProcessData(ProcessDataViewState.Dismiss)

    def notifyProcessDataError(self) -> None:
        self._notifyProcessData(ProcessDataViewState.Error)

    def _notifyPeriodSettings(self, state: PeriodSettingsViewState) -> None:
        self._periodSettingsViewStateObserver.notify(PeriodSettingsStateEvent(eventId=state))

    def _notifyActivity(
        self,
        state: ActivityViewState,
        activityName: Optional[str] = None
    ) -> None:
        self._activityViewStateObserver.notify(ActivityStateEvent(
            eventId=state,
            activityName=activityName
        ))

    def _notifyPeriodSpendRule(
        self,
        state: PeriodSpendRuleViewState,
        periods: Optional[int] = None
    ) -> None:
        self._periodSpendRuleViewStateObserver.notify(PeriodSpendRuleStateEvent(
            eventId=state,
            periods=periods
        ))

    def _notifyProcessData(
        self,
        state: ProcessDataViewState,
        result: Optional[Any] = None
    ) -> None:
        self._processDataViewStateObserver.notify(ProcessDataStateEvent(
            eventId=state,
            result=result
        ))
