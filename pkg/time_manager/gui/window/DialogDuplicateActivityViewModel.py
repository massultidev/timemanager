#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import wx
from gdev_gui_wx.ViewModelBase import ViewModelBase
from gdev_gui_wx.XrcView import XrcDialogView
from time_manager.StateEvents import ActivityStateEvent
from time_manager.ViewStates import ActivityViewState
from time_manager.gui.window.DialogDuplicateActivityModel import DialogDuplicateActivityModel


class DialogDuplicateActivityViewModel(ViewModelBase):

    def __init__(self, parent: wx.Window, model: DialogDuplicateActivityModel):
        super().__init__(
            parent=parent,
            model=model
        )

        activityStatesToSubscribe = [
            ActivityViewState.Duplicate,
            ActivityViewState.Apply,
            ActivityViewState.Dismiss,
        ]
        self._activityViewStateSubscription = self.model.stateObserver.subscribeActivityStates(
            states=activityStatesToSubscribe,
            callback=self._onActivityViewStateChange
        )

    def _createView(self, parent: wx.Window) -> wx.Window:
        view = XrcDialogView(
            parent=parent,
            resourceProvider=self.model.resourceProvider,
            viewName='DialogDuplicateActivityView'
        )

        iconPath = self.model.resourceProvider.getResourcePath('image/appicon.png')
        view.SetIcon(wx.Icon(iconPath, wx.BITMAP_TYPE_PNG))

        view.Bind(wx.EVT_CLOSE, self._onDiscard)
        view.buttonApply.Bind(wx.EVT_BUTTON, self._onApply)
        view.buttonDiscard.Bind(wx.EVT_BUTTON, self._onDiscard)

        return view

    def _loadModel(self) -> None:
        model = self.model
        self.view.textFieldName.SetValue(model.activityName)

    def _dumpModel(self) -> None:
        model = self.model
        model.activityName = self.view.textFieldName.GetValue()

    def _onActivityViewStateChange(self, event: ActivityStateEvent) -> None:
        eventId = event.eventId

        if eventId == ActivityViewState.Duplicate:
            self.model.reset()
            self.model.setEditedActivity(event.activityName)
            self.refresh()
            self._show()
            return

        if eventId in [ActivityViewState.Apply, ActivityViewState.Dismiss]:
            self._hide()

    def _onDiscard(self, event: wx.Event) -> None:
        self.model.stateObserver.notifyActivityDismiss()

    def _onApply(self, event: wx.Event) -> None:
        self._dumpModel()

        try:
            self.model.save()
        except RuntimeError as e:
            wx.MessageBox(
                caption='Error',
                message=str(e),
                style=wx.OK | wx.ICON_ERROR | wx.CENTER
            )
            return

        self.model.stateObserver.notifyActivityApply(self.model.activityName)

    def _show(self) -> None:
        self.view.Fit()
        self.view.Center()
        self.view.Show()

    def _hide(self) -> None:
        self.view.Hide()
