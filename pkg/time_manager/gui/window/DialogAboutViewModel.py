#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import wx
from gdev_gui_wx.ViewModelBase import ViewModelBase
from gdev_gui_wx.XrcView import XrcDialogView
from time_manager.gui.window.DialogAboutModel import DialogAboutModel


class DialogAboutViewModel(ViewModelBase):

    def __init__(self, parent: wx.Window, model: DialogAboutModel):
        super().__init__(
            parent=parent,
            model=model
        )

    def show(self):
        self.view.Fit()
        self.view.Center()
        self.view.ShowModal()

    def _createView(self, parent):
        return XrcDialogView(
            parent=parent,
            resourceProvider=self.model.resourceProvider,
            viewName='DialogAboutView'
        )

    def _loadModel(self):
        iconImagePath = self.model.resourceProvider.getResourcePath('image/appicon.png')

        self.view.SetIcon(wx.Icon(iconImagePath, wx.BITMAP_TYPE_PNG))

        img = wx.Image(iconImagePath, wx.BITMAP_TYPE_PNG)
        img.Rescale(120, 120, wx.IMAGE_QUALITY_HIGH)

        self.view.ctrlBmpLogo.SetBitmap(img.ConvertToBitmap())
        self.view.ctrlTitle.SetLabelText(self.model.appInfo.name)
        self.view.ctrlDescription.SetLabelText(self.model.description)
        self.view.ctrlVersion.SetLabelText(self.model.appInfo.version)
