#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from gdev_utility.ResourceProvider import ResourceProvider
from time_manager.AppInfo import AppInfo
from time_manager.PeriodSettings import PeriodSettings
from time_manager.StateObserver import StateObserver
from time_manager.activity_data.ActivityCollection import ActivityCollection


class FrameModel:

    def __init__(
        self,
        resourceProvider: ResourceProvider,
        appInfo: AppInfo,
        periodSettings: PeriodSettings,
        activityCollection: ActivityCollection,
        stateObserver: StateObserver
    ):
        self._resourceProvider = resourceProvider
        self._appInfo = appInfo
        self._periodSettings = periodSettings
        self._activityCollection = activityCollection
        self._stateObserver = stateObserver

    @property
    def resourceProvider(self) -> ResourceProvider:
        return self._resourceProvider

    @property
    def appInfo(self) -> AppInfo:
        return self._appInfo

    @property
    def periodSettings(self) -> PeriodSettings:
        return self._periodSettings

    @property
    def activityCollection(self) -> ActivityCollection:
        return self._activityCollection

    @property
    def stateObserver(self) ->StateObserver:
        return self._stateObserver
