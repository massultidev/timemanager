#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import wx
from typing import *
from gdev_gui_wx.Helpers import applyPanelOnEmptyPanel
from gdev_gui_wx.ViewModelBase import ViewModelBase
from gdev_gui_wx.XrcView import XrcFrameView
from time_manager.StateEvents import PeriodSpendRuleStateEvent, ActivityStateEvent, PeriodSettingsStateEvent, \
    ProcessDataStateEvent
from time_manager.ViewStates import ActivityViewState, PeriodSpendRuleViewState, PeriodSettingsViewState, \
    ProcessDataViewState
from time_manager.gui.panel.PanelActivityModel import PanelActivityModel
from time_manager.gui.panel.PanelActivityViewModel import PanelActivityViewModel
from time_manager.gui.window.DialogAboutModel import DialogAboutModel
from time_manager.gui.window.DialogAboutViewModel import DialogAboutViewModel
from time_manager.gui.window.DialogDuplicateActivityModel import DialogDuplicateActivityModel
from time_manager.gui.window.DialogDuplicateActivityViewModel import DialogDuplicateActivityViewModel
from time_manager.gui.window.DialogPeriodSettingsModel import DialogPeriodSettingsModel
from time_manager.gui.window.DialogPeriodSettingsViewModel import DialogPeriodSettingsViewModel
from time_manager.gui.window.DialogProgressModel import DialogProgressModel
from time_manager.gui.window.DialogProgressViewModel import DialogProgressViewModel
from time_manager.gui.window.DialogResultModel import DialogResultModel
from time_manager.gui.window.DialogResultViewModel import DialogResultViewModel
from time_manager.gui.window.FrameModel import FrameModel


class FrameViewModel(ViewModelBase):

    def __init__(self, model: FrameModel):
        self._panelActivityViewModel = None

        super().__init__(
            parent=None,
            model=model
        )

        self._dlgAbout = DialogAboutViewModel(
            parent=self.view,
            model=DialogAboutModel(
                resourceProvider=self.model.resourceProvider,
                appInfo=self.model.appInfo
            )
        )
        self._dlgPeriodSettings = DialogPeriodSettingsViewModel(
            parent=self.view,
            model=DialogPeriodSettingsModel(
                resourceProvider=self.model.resourceProvider,
                periodSettings=self.model.periodSettings,
                stateObserver=self.model.stateObserver
            )
        )
        self._dlgActivityDuplicate = DialogDuplicateActivityViewModel(
            parent=self.view,
            model=DialogDuplicateActivityModel(
                resourceProvider=self.model.resourceProvider,
                activityCollection=self.model.activityCollection,
                stateObserver=self.model.stateObserver
            )
        )
        self._dlgProgress = DialogProgressViewModel(
            parent=self.view,
            model=DialogProgressModel(
                resourceProvider=self.model.resourceProvider,
                periodSettings=self.model.periodSettings,
                activityCollection=self.model.activityCollection,
                stateObserver=self.model.stateObserver
            )
        )
        self._dlgResult = DialogResultViewModel(
            parent=self.view,
            model=DialogResultModel(
                resourceProvider=self.model.resourceProvider,
                periodSettings=self.model.periodSettings,
                stateObserver=self.model.stateObserver
            )
        )

        periodSettingsStatesToSubscribe = [e for e in PeriodSettingsViewState]
        self._PeriodSettingsViewStateSubscription = self.model.stateObserver.subscribePeriodSettingsStates(
            states=periodSettingsStatesToSubscribe,
            callback=self._onPeriodSettingsViewStateChange
        )

        activityStatesToSubscribe = [e for e in ActivityViewState]
        self._activityViewStateSubscription = self.model.stateObserver.subscribeActivityStates(
            states=activityStatesToSubscribe,
            callback=self._onActivityViewStateChange
        )

        periodSpendRuleStatesToSubscribe = [e for e in PeriodSpendRuleViewState]
        self._periodSpendRuleViewStateSubscription = self.model.stateObserver.subscribePeriodSpendRuleStates(
            states=periodSpendRuleStatesToSubscribe,
            callback=self._onPeriodSpendRuleViewStateChange
        )

        processDataViewStatesToSubscribe = [e for e in ProcessDataViewState]
        self._processDataViewStateSubscription = self.model.stateObserver.subscribeProcessDataStates(
            states=processDataViewStatesToSubscribe,
            callback=self._onProcessDataViewStateChange
        )

    def _createView(self, parent: wx.Window) -> wx.Window:
        view = XrcFrameView(
            parent=parent,
            resourceProvider=self.model.resourceProvider,
            viewName='FrameView'
        )

        iconPath = self.model.resourceProvider.getResourcePath('image/appicon.png')
        view.SetIcon(wx.Icon(iconPath, wx.BITMAP_TYPE_PNG))

        panelActivityModel = PanelActivityModel(
            resourceProvider=self.model.resourceProvider,
            activityCollection=self.model.activityCollection,
            stateObserver=self.model.stateObserver
        )
        self._panelActivityViewModel = PanelActivityViewModel(
            parent=view.panelActivity,
            model=panelActivityModel
        )
        applyPanelOnEmptyPanel(view.panelActivity, self._panelActivityViewModel.view)

        view.Bind(wx.EVT_CLOSE, self._onExit)
        view.Bind(wx.EVT_MENU, self._onAbout, id=view.getId('menuAppAbout'))
        view.Bind(wx.EVT_MENU, self._onExit, id=view.getId('menuAppExit'))
        view.Bind(wx.EVT_MENU, self._onActivityAdd, id=view.getId('menuActivityAdd'))
        view.Bind(wx.EVT_MENU, self._onActivityEdit, id=view.getId('menuActivityEdit'))
        view.Bind(wx.EVT_MENU, self._onActivityDelete, id=view.getId('menuActivityDelete'))
        view.Bind(wx.EVT_MENU, self._onActivityMoveUp, id=view.getId('menuActivityMoveUp'))
        view.Bind(wx.EVT_MENU, self._onActivityMoveDown, id=view.getId('menuActivityMoveDown'))
        view.Bind(wx.EVT_MENU, self._onActivityDuplicate, id=view.getId('menuActivityDuplicate'))
        view.Bind(wx.EVT_MENU, self._onActivityDeleteAll, id=view.getId('menuActivityDeleteAll'))
        view.Bind(wx.EVT_MENU, self._onSetPeriods, id=view.getId('menuRunSetPeriods'))
        view.Bind(wx.EVT_MENU, self._onCalculateResult, id=view.getId('menuRunCalculateResult'))
        view.buttonActivityAdd.Bind(wx.EVT_BUTTON, self._onActivityAdd)
        view.buttonActivityEdit.Bind(wx.EVT_BUTTON, self._onActivityEdit)
        view.buttonActivityDuplicate.Bind(wx.EVT_BUTTON, self._onActivityDuplicate)
        view.buttonActivityDelete.Bind(wx.EVT_BUTTON, self._onActivityDelete)
        view.buttonActivityDeleteAll.Bind(wx.EVT_BUTTON, self._onActivityDeleteAll)
        view.buttonActivityMoveUp.Bind(wx.EVT_BUTTON, self._onActivityMoveUp)
        view.buttonActivityMoveDown.Bind(wx.EVT_BUTTON, self._onActivityMoveDown)
        view.listBoxActivityNames.Bind(wx.EVT_LISTBOX, self._onActivityFocus)
        view.labelPeriodsText.Bind(wx.EVT_LEFT_UP, self._onSetPeriods)
        view.textFieldPeriodsValue.Bind(wx.EVT_LEFT_UP, self._onSetPeriods)
        view.labelUnitText.Bind(wx.EVT_LEFT_UP, self._onSetPeriods)
        view.textFieldPeriodsUnit.Bind(wx.EVT_LEFT_UP, self._onSetPeriods)

        return view

    def _loadModel(self) -> None:
        self.view.listBoxActivityNames.Clear()
        for activityName in self.model.activityCollection.keys():
            self.view.listBoxActivityNames.Append(activityName)

        periodSettings = self.model.periodSettings
        self.view.textFieldPeriodsValue.SetValue(str(periodSettings.periodCount))
        self.view.textFieldPeriodsUnit.SetValue(periodSettings.periodUnit)

        self._refreshActivityPanel()

        self.view.Fit()

    def _onPeriodSettingsViewStateChange(self, event: PeriodSettingsStateEvent) -> None:
        eventId = event.eventId

        if eventId == PeriodSettingsViewState.Edit:
            self._enable(enable=False)
            return

        if eventId == PeriodSettingsViewState.Apply:
            self.refresh()

        self._enable(enable=True)

    def _onActivityViewStateChange(self, event: ActivityStateEvent) -> None:
        eventId = event.eventId

        if eventId in [ActivityViewState.Apply]:
            self.refresh()
        elif eventId in [ActivityViewState.Dismiss]:
            self._refreshActivityPanel()

        if eventId in [ActivityViewState.Add, ActivityViewState.Edit]:
            self._enableActivityControls(enable=False)
        else:
            self._enableActivityControls(enable=True)

        if eventId in [ActivityViewState.Duplicate]:
            self._enable(enable=False)
        else:
            self._enable(enable=True)

    def _onPeriodSpendRuleViewStateChange(self, event: PeriodSpendRuleStateEvent) -> None:
        enable = (event.eventId not in [PeriodSpendRuleViewState.Add, PeriodSpendRuleViewState.Edit])
        self._enable(enable=enable)

    def _onProcessDataViewStateChange(self, event: ProcessDataStateEvent) -> None:
        eventId = event.eventId
        enable = (eventId in [ProcessDataViewState.Dismiss, ProcessDataViewState.Error])
        self._enable(enable=enable)

        if eventId == ProcessDataViewState.Error:
            wx.MessageBox(
                caption='Error',
                message='Processing activity data failed!',
                style=wx.OK | wx.ICON_ERROR | wx.CENTER
            )

    def _onExit(self, event: wx.Event) -> None:
        answer = wx.MessageBox(
            caption='Confirmation',
            message='Are you sure you want to quit?',
            style=wx.OK | wx.CANCEL | wx.CANCEL_DEFAULT | wx.ICON_QUESTION | wx.CENTER
        )
        if answer == wx.CANCEL:
            return

        self.view.Destroy()

    def _onAbout(self, event: wx.Event) -> None:
        self._dlgAbout.show()

    def _onActivityFocus(self, event: wx.Event) -> None:
        self._refreshActivityPanel()

    def _onActivityAdd(self, event: wx.Event) -> None:
        self.model.stateObserver.notifyActivityAdd()

    def _onActivityEdit(self, event: wx.Event) -> None:
        selectedActivityName = self._getSelectedActivityName()
        if selectedActivityName is None:
            return

        self.model.stateObserver.notifyActivityEdit(selectedActivityName)

    def _onActivityDelete(self, event: wx.Event) -> None:
        selectedActivityName = self._getSelectedActivityName()
        if selectedActivityName is None:
            return

        answer = wx.MessageBox(
            caption='Confirmation',
            message='Are you sure you want to delete "{}" activity?'.format(selectedActivityName),
            style=wx.OK | wx.CANCEL | wx.CANCEL_DEFAULT | wx.ICON_QUESTION | wx.CENTER
        )
        if answer == wx.CANCEL:
            return

        self.model.activityCollection.remove(selectedActivityName)
        self.refresh()

    def _onActivityDeleteAll(self, event: wx.Event) -> None:
        if len(self.model.activityCollection) == 0:
            return

        answer = wx.MessageBox(
            caption='Confirmation',
            message='Are you sure you want to clear activities list?',
            style=wx.OK | wx.CANCEL | wx.CANCEL_DEFAULT | wx.ICON_QUESTION | wx.CENTER
        )
        if answer == wx.CANCEL:
            return

        self.model.activityCollection.clear()
        self.refresh()

    def _onActivityMoveUp(self, event: wx.Event) -> None:
        selectedActivityName = self._getSelectedActivityName()
        if selectedActivityName is None:
            return

        self.model.activityCollection.moveUp(selectedActivityName)
        self.refresh()
        self._selectActivity(selectedActivityName)

    def _onActivityMoveDown(self, event: wx.Event) -> None:
        selectedActivityName = self._getSelectedActivityName()
        if selectedActivityName is None:
            return

        self.model.activityCollection.moveDown(selectedActivityName)
        self.refresh()
        self._selectActivity(selectedActivityName)

    def _onActivityDuplicate(self, event: wx.Event) -> None:
        selectedActivityName = self._getSelectedActivityName()
        if selectedActivityName is None:
            return

        self.model.stateObserver.notifyActivityDuplicate(selectedActivityName)

    def _onSetPeriods(self, event: wx.Event) -> None:
        event.Skip()
        self.model.stateObserver.notifyPeriodSettingsEdit()

    def _onCalculateResult(self, event: wx.Event) -> None:
        if len(self.model.activityCollection) == 0:
            wx.MessageBox(
                caption='Warning',
                message='There is nothing to process!\nList of activities is empty.',
                style=wx.OK | wx.ICON_WARNING | wx.CENTER
            )
            return

        periodSettings = self.model.periodSettings
        if periodSettings.periodCount == 0:
            wx.MessageBox(
                caption='Warning',
                message='There is nothing to process!\nNo "{}" to spend.'.format(periodSettings.periodUnit),
                style=wx.OK | wx.ICON_WARNING | wx.CENTER
            )
            return

        self.model.stateObserver.notifyProcessDataProcess()

    def _refreshActivityPanel(self) -> None:
        selectedActivityName = self._getSelectedActivityName()
        if selectedActivityName is None:
            return

        self.model.stateObserver.notifyActivityDisplay(selectedActivityName)

    def _enable(self, enable: Optional[bool] = True) -> None:
        self.view.Enable(enable)
        if enable:
            self.view.Raise()
            self.view.SetFocus()

    def _enableActivityControls(self, enable: Optional[bool] = True) -> None:
        menuBar = self.view.GetMenuBar()
        for menuIndex in range(menuBar.GetMenuCount()):
            menuBar.EnableTop(menuIndex, enable)
        self.view.panelActivityControl.Enable(enable)

    def _getSelectedActivityName(self) -> Optional[str]:
        index = self.view.listBoxActivityNames.GetSelection()
        if index == wx.NOT_FOUND:
            return None

        return self.view.listBoxActivityNames.GetString(index)

    def _selectActivity(self, activityName: str) -> None:
        self.view.listBoxActivityNames.SetStringSelection(activityName)
