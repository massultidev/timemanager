#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from gdev_utility.ResourceProvider import ResourceProvider
from time_manager.PeriodSettings import PeriodSettings
from time_manager.StateObserver import StateObserver


class DialogPeriodSettingsModel:

    def __init__(
        self,
        resourceProvider: ResourceProvider,
        periodSettings: PeriodSettings,
        stateObserver: StateObserver
    ):
        self._resourceProvider = resourceProvider
        self._periodSettings = periodSettings
        self._stateObserver = stateObserver

    @property
    def resourceProvider(self) -> ResourceProvider:
        return self._resourceProvider

    @property
    def periodSettings(self):
        return self._periodSettings

    @property
    def stateObserver(self) -> StateObserver:
        return self._stateObserver
