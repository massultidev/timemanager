#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import wx
from gdev_gui_wx.ViewModelBase import ViewModelBase
from gdev_gui_wx.XrcView import XrcDialogView
from time_manager.StateEvents import PeriodSpendRuleStateEvent
from time_manager.ViewStates import PeriodSpendRuleViewState
from time_manager.activity_data.PeriodSpendRule import PeriodSpendRule
from time_manager.gui.window.DialogPeriodSpendRuleModel import DialogPeriodSpendRuleModel


class DialogPeriodSpendRuleViewModel(ViewModelBase):

    def __init__(self, parent: wx.Window, model: DialogPeriodSpendRuleModel):
        super().__init__(
            parent=parent,
            model=model
        )

        periodSpendRuleStatesToSubscribe = [e for e in PeriodSpendRuleViewState]
        self._periodSpendRuleViewStateSubscription = self.model.stateObserver.subscribePeriodSpendRuleStates(
            states=periodSpendRuleStatesToSubscribe,
            callback=self._onPeriodSpendRuleViewStateChange
        )

    def _createView(self, parent: wx.Window) -> wx.Window:
        view = XrcDialogView(
            parent=parent,
            resourceProvider=self.model.resourceProvider,
            viewName='DialogPeriodSpendRuleView'
        )

        view.Bind(wx.EVT_CLOSE, self._onCancel)
        view.buttonApply.Bind(wx.EVT_BUTTON, self._onApply)
        view.buttonCancel.Bind(wx.EVT_BUTTON, self._onCancel)

        return view

    def _loadModel(self) -> None:
        rule = self.model.rule

        self.view.spinValue.SetMax(PeriodSpendRule.MAX_POINTS_PER_RULE)

        self.view.spinPeriods.SetValue(rule.periods)
        self.view.spinValue.SetValue(rule.valueToGain)

    def _dumpModel(self) -> None:
        rule = self.model.rule
        rule.periods = int(self.view.spinPeriods.GetValue())
        rule.valueToGain = int(self.view.spinValue.GetValue())

    def _enablePeriodsModification(self, enable: bool = True) -> None:
        self.view.spinPeriods.Enable(enable)

    def _onPeriodSpendRuleViewStateChange(self, event: PeriodSpendRuleStateEvent) -> None:
        eventId = event.eventId

        if eventId in [PeriodSpendRuleViewState.Add, PeriodSpendRuleViewState.Edit]:
            self.model.reset()
            if eventId == PeriodSpendRuleViewState.Add:
                self._enablePeriodsModification(enable=True)
            elif eventId == PeriodSpendRuleViewState.Edit:
                self._enablePeriodsModification(enable=False)
                self.model.setEditedRule(event.periods)
            self.refresh()
            self._show()
            return

        if eventId in [PeriodSpendRuleViewState.Apply, PeriodSpendRuleViewState.Dismiss]:
            self._hide()

    def _onCancel(self, event: wx.Event) -> None:
        self.model.stateObserver.notifyPeriodSpendRuleDismiss()

    def _onApply(self, event: wx.Event) -> None:
        self._dumpModel()

        try:
            self.model.save()
        except RuntimeError as e:
            wx.MessageBox(
                caption='Error',
                message=str(e),
                style=wx.OK | wx.ICON_ERROR | wx.CENTER
            )
            return

        self.model.stateObserver.notifyPeriodSpendRuleApply(periods=self.model.rule.periods)

    def _show(self) -> None:
        self.view.Fit()
        self.view.Center()
        self.view.Show()

    def _hide(self) -> None:
        self.view.Hide()
