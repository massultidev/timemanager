#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import wx
from gdev_gui_wx.ViewModelBase import ViewModelBase
from gdev_gui_wx.XrcView import XrcDialogView
from time_manager.StateEvents import PeriodSettingsStateEvent
from time_manager.ViewStates import PeriodSettingsViewState
from time_manager.gui.window.DialogPeriodSettingsModel import DialogPeriodSettingsModel


class DialogPeriodSettingsViewModel(ViewModelBase):

    def __init__(self, parent: wx.Window, model: DialogPeriodSettingsModel):
        super().__init__(
            parent=parent,
            model=model
        )

        periodSettingsStatesToSubscribe = [e for e in PeriodSettingsViewState]
        self._PeriodSettingsViewStateSubscription = self.model.stateObserver.subscribePeriodSettingsStates(
            states=periodSettingsStatesToSubscribe,
            callback=self._onPeriodSettingsViewStateChange
        )

    def _createView(self, parent: wx.Window) -> wx.Window:
        view = XrcDialogView(
            parent=parent,
            resourceProvider=self.model.resourceProvider,
            viewName='DialogPeriodSettingsView'
        )

        view.Bind(wx.EVT_CLOSE, self._onCancel)
        view.buttonApply.Bind(wx.EVT_BUTTON, self._onApply)
        view.buttonCancel.Bind(wx.EVT_BUTTON, self._onCancel)

        return view

    def _loadModel(self) -> None:
        periodSettings = self.model.periodSettings

        self.view.spinAmount.SetValue(periodSettings.periodCount)
        self.view.textFieldUnit.SetValue(periodSettings.periodUnit)

    def _dumpModel(self) -> None:
        periodSettings = self.model.periodSettings
        periodSettings.periodCount = self.view.spinAmount.GetValue()
        periodSettings.periodUnit = self.view.textFieldUnit.GetValue()

    def _onPeriodSettingsViewStateChange(self, event: PeriodSettingsStateEvent) -> None:
        eventId = event.eventId

        if eventId == PeriodSettingsViewState.Edit:
            self.refresh()
            self._show()
            return

        self._hide()

    def _onCancel(self, event: wx.Event) -> None:
        self.model.stateObserver.notifyPeriodSettingsDismiss()

    def _onApply(self, event: wx.Event) -> None:
        self._dumpModel()
        self.model.stateObserver.notifyPeriodSettingsApply()

    def _show(self) -> None:
        self.view.Fit()
        self.view.Center()
        self.view.Show()

    def _hide(self) -> None:
        self.view.Hide()
