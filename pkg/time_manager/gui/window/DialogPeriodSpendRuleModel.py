#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import copy
from gdev_utility.ResourceProvider import ResourceProvider
from time_manager.StateObserver import StateObserver
from time_manager.activity_data.PeriodSpendRule import PeriodSpendRule
from time_manager.activity_data.PeriodSpendRuleCollection import PeriodSpendRuleCollection


class DialogPeriodSpendRuleModel:

    def __init__(
        self,
        resourceProvider: ResourceProvider,
        stateObserver: StateObserver
    ):
        self._resourceProvider = resourceProvider
        self._stateObserver = stateObserver

        self._rules = None

        self._ruleNew = None
        self._ruleEdited = None

        self.reset()

    @property
    def resourceProvider(self) -> ResourceProvider:
        return self._resourceProvider

    @property
    def stateObserver(self) -> StateObserver:
        return self._stateObserver

    @property
    def rules(self) -> PeriodSpendRuleCollection:
        return self._rules

    @rules.setter
    def rules(self, value: PeriodSpendRuleCollection) -> None:
        self.reset()
        self._rules = value

    @property
    def rule(self) -> PeriodSpendRule:
        return self._ruleNew

    def reset(self) -> None:
        self._ruleNew = PeriodSpendRule()
        self._ruleEdited = None

    def setEditedRule(self, periods: int) -> None:
        self._ruleEdited = self._rules.get(periods)
        self._ruleNew = copy.deepcopy(self._ruleEdited)

    def save(self) -> None:
        if self._ruleNew.periods == 0:
            raise RuntimeError('Cannot create a rule for no periods spend!')

        if self._ruleEdited is not None:
            if self._ruleNew == self._ruleEdited:
                return

            self._updateRule()
            return

        self._addRule()

    def _addRule(self) -> None:
        self._rules.add(self._ruleNew)

    def _updateRule(self) -> None:
        if self._ruleNew.periods == self._ruleEdited.periods:
            self._rules.update(self._ruleNew)
            return

        if self._rules.has(self._ruleNew.periods):
            raise RuntimeError('Rule for {} periods already exists!'.format(self._ruleNew.periods))

        self._rules.add(self._ruleNew)
