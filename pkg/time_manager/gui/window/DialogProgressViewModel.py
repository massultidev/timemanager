#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import wx
import wx.lib.newevent
from threading import Thread
from gdev_gui_wx.ViewModelBase import ViewModelBase
from gdev_gui_wx.XrcView import XrcDialogView
from gdev_utility.Loggable import Loggable
from time_manager.StateEvents import ProcessDataStateEvent
from time_manager.ViewStates import ProcessDataViewState
from time_manager.gui.window.DialogProgressModel import DialogProgressModel

EventProcessingFinished, EVT_PROCESSING_FINISHED = wx.lib.newevent.NewEvent()
EventProcessingError, EVT_PROCESSING_ERROR = wx.lib.newevent.NewEvent()


class DialogProgressViewModel(ViewModelBase, Loggable):

    def __init__(self, parent: wx.Window, model: DialogProgressModel):
        super().__init__(
            parent=parent,
            model=model
        )

        processDataViewStatesToSubscribe = [e for e in ProcessDataViewState]
        self._processDataViewStateSubscription = self.model.stateObserver.subscribeProcessDataStates(
            states=processDataViewStatesToSubscribe,
            callback=self._onProcessDataViewStateChange
        )

    def _createView(self, parent: wx.Window) -> wx.Window:
        view = XrcDialogView(
            parent=parent,
            resourceProvider=self.model.resourceProvider,
            viewName='DialogProgressView'
        )

        iconPath = self.model.resourceProvider.getResourcePath('image/appicon.png')
        view.SetIcon(wx.Icon(iconPath, wx.BITMAP_TYPE_PNG))

        view.Bind(wx.EVT_CLOSE, self._onDismiss)
        view.Bind(EVT_PROCESSING_FINISHED, self._onFinish)
        view.Bind(EVT_PROCESSING_ERROR, self._onError)

        return view

    def _loadModel(self) -> None:
        self.view.SetTitle('Processing')
        self.view.labelInformation.SetLabel('Searching for best results...')
        self.view.progressBar.SetValue(0)

    def _onProcessDataViewStateChange(self, event: ProcessDataStateEvent) -> None:
        eventId = event.eventId

        if eventId == ProcessDataViewState.Process:
            self.refresh()
            self._show()
            self._start()
            return

        self._hide()

    def _onDismiss(self, event: wx.Event) -> None:
        pass

    def _onFinish(self, event: EventProcessingFinished) -> None:
        self.model.stateObserver.notifyProcessDataDisplay(self.model.result)

    def _onError(self, event: EventProcessingError) -> None:
        self.model.stateObserver.notifyProcessDataError()

    def _show(self) -> None:
        self.view.Fit()
        self.view.Center()
        self.view.Show()

    def _hide(self) -> None:
        self.view.Hide()

    def _start(self) -> None:
        Thread(
            name='DialogProgressThread',
            target=self._run
        ).start()

    def _run(self) -> None:
        try:
            progressBar = self.view.progressBar
            progressBar.Pulse()
            self.model.calculateResult()
            progressBar.SetValue(progressBar.GetRange())
            self.view.QueueEvent(EventProcessingFinished())
            return

        except Exception as e:
            self.log.error(e)

        self.view.QueueEvent(EventProcessingError())
