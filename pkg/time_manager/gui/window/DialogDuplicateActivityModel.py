#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import copy
from gdev_utility.ResourceProvider import ResourceProvider
from time_manager.StateObserver import StateObserver
from time_manager.activity_data.ActivityCollection import ActivityCollection


class DialogDuplicateActivityModel:

    def __init__(
        self,
        resourceProvider: ResourceProvider,
        activityCollection: ActivityCollection,
        stateObserver: StateObserver
    ):
        self._resourceProvider = resourceProvider
        self._activityCollection = activityCollection
        self._stateObserver = stateObserver

        self._activityNameNew = None
        self._activityNameEdited = None

        self.reset()

    @property
    def resourceProvider(self) -> ResourceProvider:
        return self._resourceProvider

    @property
    def activityCollection(self) -> ActivityCollection:
        return self._activityCollection

    @property
    def stateObserver(self) -> StateObserver:
        return self._stateObserver

    @property
    def activityName(self) -> str:
        return self._activityNameNew

    @activityName.setter
    def activityName(self, value: str) -> None:
        self._activityNameNew = value

    def reset(self) -> None:
        self._activityNameNew = ''
        self._activityNameEdited = None

    def setEditedActivity(self, activityName: str) -> None:
        self._activityNameEdited = activityName
        self._activityNameNew = activityName

    def save(self) -> None:
        if len(self._activityNameNew) == 0:
            raise RuntimeError('Activity must have a name!')

        if self._activityCollection.has(self._activityNameNew):
            raise RuntimeError('Activity "{}" already exists!'.format(self._activityNameNew))

        activity = copy.deepcopy(self._activityCollection.get(self._activityNameEdited))
        activity.name = self._activityNameNew
        self._activityCollection.add(activity)
