#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import wx
from typing import *
from gdev_gui_wx.Helpers import applyPanelOnEmptyPanel
from gdev_gui_wx.ViewModelBase import ViewModelBase
from gdev_gui_wx.XrcView import XrcDialogView
from time_manager.StateEvents import ProcessDataStateEvent
from time_manager.ViewStates import ProcessDataViewState
from time_manager.gui.panel.PanelResultModel import PanelResultModel
from time_manager.gui.panel.PanelResultViewModel import PanelResultViewModel
from time_manager.gui.window.DialogResultModel import DialogResultModel


class DialogResultViewModel(ViewModelBase):

    def __init__(self, parent: wx.Window, model: DialogResultModel):
        self._panelResult = None

        super().__init__(
            parent=parent,
            model=model
        )

        processDataViewStatesToSubscribe = [e for e in ProcessDataViewState]
        self._processDataViewStateSubscription = self.model.stateObserver.subscribeProcessDataStates(
            states=processDataViewStatesToSubscribe,
            callback=self._onProcessDataViewStateChange
        )

    def _createView(self, parent: wx.Window) -> wx.Window:
        view = XrcDialogView(
            parent=parent,
            resourceProvider=self.model.resourceProvider,
            viewName='DialogResultView'
        )

        self._panelResult = PanelResultViewModel(
            parent=view.panelSolution,
            model=PanelResultModel(
                periodSettings=self.model.periodSettings
            )
        )
        applyPanelOnEmptyPanel(parent=view.panelSolution, child=self._panelResult.view)

        view.Bind(wx.EVT_CLOSE, self._onDismiss)
        view.listSolutions.Bind(wx.EVT_LISTBOX, self._onSelect)
        view.buttonOk.Bind(wx.EVT_BUTTON, self._onDismiss)

        return view

    def _loadModel(self) -> None:
        self.view.listSolutions.Clear()
        resultsSize = len(self.model.results)
        for i in range(resultsSize):
            self.view.listSolutions.Append(str(i))

        self.view.listSolutions.SetFocus()
        if resultsSize > 0:
            self.view.listSolutions.SetSelection(0)

        self._refreshResultPanel()

    def _onProcessDataViewStateChange(self, event: ProcessDataStateEvent) -> None:
        eventId = event.eventId

        if eventId == ProcessDataViewState.Display:
            self.model.results = event.result
            self.refresh()
            self._show()
            return

        self._hide()

    def _onSelect(self, event: wx.Event) -> None:
        self._refreshResultPanel()

    def _onDismiss(self, event: wx.Event) -> None:
        self.model.stateObserver.notifyProcessDataDismiss()

    def _show(self) -> None:
        self.view.Fit()
        self.view.Center()
        self.view.Show()

    def _hide(self) -> None:
        self.view.Hide()

    def _refreshResultPanel(self) -> None:
        self._panelResult.model.reset()
        resultIndex = self._getSelectedResult()
        if resultIndex is not None:
            self._panelResult.model.result = self.model.results[resultIndex]
        self._panelResult.refresh()

    def _getSelectedResult(self) -> Optional[int]:
        index = self.view.listSolutions.GetSelection()
        if index == wx.NOT_FOUND:
            return None
        return index
