#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from typing import *

from gdev_utility.Loggable import Loggable
from gdev_utility.ResourceProvider import ResourceProvider
from time_manager.PeriodSettings import PeriodSettings
from time_manager.StateObserver import StateObserver
from time_manager.activity_data.Activity import Activity
from time_manager.activity_data.ActivityCollection import ActivityCollection
from time_manager.activity_data.PeriodSpendRule import PeriodSpendRule
from time_manager.algorithm.ActivityData import ActivityData
from time_manager.algorithm.ActivityDataProcessor import ActivityDataProcessor


class PeriodsSpendDecision:
    def __init__(self, activityName: str, spentPeriods: int):
        self._activityName = activityName
        self._spentPeriods = spentPeriods

    @property
    def activityName(self) -> str:
        return self._activityName

    @property
    def spentPeriods(self) -> int:
        return self._spentPeriods


class ActivityProcessorResult:
    def __init__(self, sparePeriods: int, periodSpendDecisions: List[PeriodsSpendDecision]):
        self._sparePeriods = sparePeriods
        self._periodSpendDecisions = periodSpendDecisions

    @property
    def sparePeriods(self) -> int:
        return self._sparePeriods

    @property
    def periodSpendDecisions(self) -> List[PeriodsSpendDecision]:
        return self._periodSpendDecisions


class DialogProgressModel(Loggable):

    def __init__(
            self,
            resourceProvider: ResourceProvider,
            periodSettings: PeriodSettings,
            activityCollection: ActivityCollection,
            stateObserver: StateObserver
    ):
        self._resourceProvider = resourceProvider
        self._periodSettings = periodSettings
        self._activityCollection = activityCollection
        self._stateObserver = stateObserver

        self._result = None

        self._reset()

    @property
    def resourceProvider(self) -> ResourceProvider:
        return self._resourceProvider

    @property
    def periodSettings(self) -> PeriodSettings:
        return self._periodSettings

    @property
    def activityCollection(self) -> ActivityCollection:
        return self._activityCollection

    @property
    def stateObserver(self) -> StateObserver:
        return self._stateObserver

    @property
    def result(self) -> List[ActivityProcessorResult]:
        return self._result

    # FIXME: Deprecated!
    def calculateResult(self) -> None:
        self._reset()

        activityProcessor = ActivityDataProcessor(
            activities=self._activitiesAsRawData(),
            periods=self._periodSettings.periodCount
        )
        activityProcessor.run()

        activityNames = list(self._activityCollection.keys())
        for sparePeriods, periodSpendDecisionsRaw in activityProcessor.getResult():
            periodSpendDecisions = []
            for i in range(len(periodSpendDecisionsRaw)):
                decision = PeriodsSpendDecision(activityNames[i], periodSpendDecisionsRaw[i])
                periodSpendDecisions.append(decision)

            result = ActivityProcessorResult(
                sparePeriods=sparePeriods,
                periodSpendDecisions=periodSpendDecisions
            )
            self._result.append(result)

    def _reset(self) -> None:
        self._result = []

    def _activitiesAsRawData(self) -> List[ActivityData]:
        requiredPenaltyValue = -(self._periodSettings.periodCount * PeriodSpendRule.MAX_POINTS_PER_RULE)

        self.log.info('Calculated penalty value: "{}"'.format(requiredPenaltyValue))

        activities = []
        for activity in self._activityCollection.values():
            activities.append(self._activityAsRawData(activity, requiredPenaltyValue))

        return activities

    def _activityAsRawData(self, activity: Activity, requiredPenaltyValue: int) -> ActivityData:
        rules = {rule.periods: rule.valueToGain for rule in activity.periodSpendRules.values()}
        rules[0] = 0 if not activity.isRequired else requiredPenaltyValue
        return ActivityData(rules)
