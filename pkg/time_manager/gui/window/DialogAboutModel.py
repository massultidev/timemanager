#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from gdev_utility.ResourceProvider import ResourceProvider
from time_manager.AppInfo import AppInfo


class DialogAboutModel:

    def __init__(
        self,
        resourceProvider: ResourceProvider,
        appInfo: AppInfo
    ):
        self._resourceProvider = resourceProvider
        self._appInfo = appInfo

    @property
    def resourceProvider(self) -> ResourceProvider:
        return self._resourceProvider

    @property
    def appInfo(self):
        return self._appInfo

    @property
    def description(self):
        return "Application for time management.\n" \
               "Implemented with a use of dynamic\n" \
               "programming algorithm."
