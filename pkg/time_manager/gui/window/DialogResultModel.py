#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from typing import *
from gdev_utility.ResourceProvider import ResourceProvider
from time_manager.PeriodSettings import PeriodSettings
from time_manager.StateObserver import StateObserver
from time_manager.gui.window.DialogProgressModel import ActivityProcessorResult


class DialogResultModel:

    def __init__(
        self,
        resourceProvider: ResourceProvider,
        periodSettings: PeriodSettings,
        stateObserver: StateObserver
    ):
        self._resourceProvider = resourceProvider
        self._periodSettings = periodSettings
        self._stateObserver = stateObserver

        self._results = None

        self.reset()

    @property
    def resourceProvider(self) -> ResourceProvider:
        return self._resourceProvider

    @property
    def periodSettings(self) -> PeriodSettings:
        return self._periodSettings

    @property
    def stateObserver(self) -> StateObserver:
        return self._stateObserver

    @property
    def results(self) -> List[ActivityProcessorResult]:
        return self._results

    @results.setter
    def results(self, value: List[ActivityProcessorResult]) -> None:
        self._results = value

    def reset(self) -> None:
        self._results = []
