#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from time_manager.PeriodSettings import PeriodSettings
from time_manager.gui.window.DialogProgressModel import ActivityProcessorResult


class PanelResultModel:

    def __init__(
        self,
        periodSettings: PeriodSettings,
    ):
        self._periodSettings = periodSettings

        self._result = None

        self.reset()

    @property
    def periodSettings(self) -> PeriodSettings:
        return self._periodSettings

    @property
    def result(self) -> ActivityProcessorResult:
        return self._result

    @result.setter
    def result(self, value: ActivityProcessorResult) -> None:
        self._result = value

    def reset(self) -> None:
        self._result = ActivityProcessorResult(
            sparePeriods=self._periodSettings.periodCount,
            periodSpendDecisions=[]
        )
