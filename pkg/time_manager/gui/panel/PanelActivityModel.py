#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import copy
from gdev_utility.ResourceProvider import ResourceProvider
from time_manager.StateObserver import StateObserver
from time_manager.activity_data.Activity import Activity
from time_manager.activity_data.ActivityCollection import ActivityCollection


class PanelActivityModel:

    def __init__(
        self,
        resourceProvider: ResourceProvider,
        activityCollection: ActivityCollection,
        stateObserver: StateObserver
    ):
        self._resourceProvider = resourceProvider
        self._activityCollection = activityCollection
        self._stateObserver = stateObserver

        self._activityNew = None
        self._activityEdited = None

        self.reset()

    @property
    def resourceProvider(self) -> ResourceProvider:
        return self._resourceProvider

    @property
    def activityCollection(self) -> ActivityCollection:
        return self._activityCollection

    @property
    def stateObserver(self) -> StateObserver:
        return self._stateObserver

    @property
    def activity(self) -> Activity:
        return self._activityNew

    def reset(self) -> None:
        self._activityNew = Activity()
        self._activityEdited = None

    def setEditedActivity(self, activityName: str) -> None:
        self._activityEdited = self._activityCollection.get(activityName)
        self._activityNew = copy.deepcopy(self._activityEdited)

    def save(self) -> None:
        if len(self._activityNew.name) == 0:
            raise RuntimeError('Activity must have a name!')

        if len(self._activityNew.periodSpendRules) == 0:
            raise RuntimeError('Activity must have period spend rules!')

        if self._activityEdited is not None:
            if self._activityNew == self._activityEdited:
                return

            self._updateActivity()
            return

        self._addActivity()

    def _addActivity(self) -> None:
        if self._activityCollection.has(self._activityNew.name):
            raise RuntimeError('Activity "{}" already exists!'.format(self._activityNew.name))

        self._activityCollection.add(self._activityNew)

    def _updateActivity(self) -> None:
        if self._activityNew.name == self._activityEdited.name:
            self._activityCollection.update(self._activityNew)
            return

        if self._activityCollection.has(self._activityNew.name):
            raise RuntimeError('Activity "{}" already exists!'.format(self._activityNew.name))

        self._activityCollection.add(self._activityNew)
