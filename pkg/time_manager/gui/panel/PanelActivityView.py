#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import wx
import wx.dataview


class PanelActivityView(wx.Panel):

    def __init__(self, parent: wx.Window):
        super().__init__(parent=parent)

        self.SetMinSize(wx.Size(250, -1))

        sizer = wx.BoxSizer(wx.VERTICAL)

        sizerBoxActivity = wx.StaticBoxSizer(wx.StaticBox(
            self, wx.ID_ANY, u"Activity:"), wx.VERTICAL)

        sizerName = wx.BoxSizer(wx.HORIZONTAL)

        self.labelName = wx.StaticText(
            sizerBoxActivity.GetStaticBox(),
            wx.ID_ANY,
            u"Name:",
            wx.DefaultPosition,
            wx.DefaultSize,
            0)
        self.labelName.Wrap(-1)

        sizerName.Add(self.labelName, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.textFieldName = wx.TextCtrl(
            sizerBoxActivity.GetStaticBox(),
            wx.ID_ANY,
            wx.EmptyString,
            wx.DefaultPosition,
            wx.DefaultSize,
            0)
        sizerName.Add(self.textFieldName, 1, wx.ALL |
                      wx.ALIGN_CENTER_VERTICAL, 5)

        sizerBoxActivity.Add(sizerName, 0, wx.EXPAND, 5)

        self.checkBoxIsRequired = wx.CheckBox(
            sizerBoxActivity.GetStaticBox(),
            wx.ID_ANY,
            u"Is this activity required?",
            wx.DefaultPosition,
            wx.DefaultSize,
            0)
        sizerBoxActivity.Add(self.checkBoxIsRequired, 0,
                             wx.BOTTOM | wx.RIGHT | wx.LEFT, 5)

        sizer.Add(sizerBoxActivity, 0, wx.EXPAND, 5)

        sizerBoxRule = wx.StaticBoxSizer(wx.StaticBox(
            self, wx.ID_ANY, u"Rules:"), wx.HORIZONTAL)

        sizerRuleControl = wx.BoxSizer(wx.VERTICAL)

        self.buttonAddRule = wx.Button(
            sizerBoxRule.GetStaticBox(),
            wx.ID_ANY,
            wx.EmptyString,
            wx.DefaultPosition,
            wx.DefaultSize,
            wx.BU_EXACTFIT)

        self.buttonAddRule.SetBitmap(
            wx.ArtProvider.GetBitmap(
                wx.ART_PLUS, wx.ART_BUTTON))
        self.buttonAddRule.SetToolTip(u"Add period spending rule.")

        sizerRuleControl.Add(self.buttonAddRule, 0, wx.BOTTOM, 5)

        self.buttonEditRule = wx.Button(
            sizerBoxRule.GetStaticBox(),
            wx.ID_ANY,
            wx.EmptyString,
            wx.DefaultPosition,
            wx.DefaultSize,
            wx.BU_EXACTFIT)

        self.buttonEditRule.SetBitmap(
            wx.ArtProvider.GetBitmap(
                wx.ART_FILE_OPEN, wx.ART_BUTTON))
        self.buttonEditRule.SetToolTip(u"Edit period spending rule.")

        sizerRuleControl.Add(self.buttonEditRule, 0, wx.BOTTOM, 5)

        self.buttonDeleteRule = wx.Button(
            sizerBoxRule.GetStaticBox(),
            wx.ID_ANY,
            wx.EmptyString,
            wx.DefaultPosition,
            wx.DefaultSize,
            wx.BU_EXACTFIT)

        self.buttonDeleteRule.SetBitmap(
            wx.ArtProvider.GetBitmap(
                wx.ART_MINUS, wx.ART_BUTTON))
        self.buttonDeleteRule.SetToolTip(u"Delete period spending rule.")

        sizerRuleControl.Add(self.buttonDeleteRule, 0, wx.BOTTOM, 5)

        self.buttonDeleteAllRules = wx.Button(
            sizerBoxRule.GetStaticBox(),
            wx.ID_ANY,
            wx.EmptyString,
            wx.DefaultPosition,
            wx.DefaultSize,
            wx.BU_EXACTFIT)

        self.buttonDeleteAllRules.SetBitmap(
            wx.ArtProvider.GetBitmap(
                wx.ART_CROSS_MARK, wx.ART_BUTTON))
        self.buttonDeleteAllRules.SetToolTip(
            u"Delete all period spending rules.")

        sizerRuleControl.Add(self.buttonDeleteAllRules, 0, wx.BOTTOM, 5)

        sizerBoxRule.Add(
            sizerRuleControl,
            0,
            wx.EXPAND | wx.RIGHT | wx.LEFT,
            5)

        self.dataViewRules = wx.dataview.DataViewListCtrl(
            sizerBoxRule.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0)
        self.columnPeriods = self.dataViewRules.AppendTextColumn(
            u"Periods", wx.dataview.DATAVIEW_CELL_INERT, -1, wx.ALIGN_LEFT, wx.dataview.DATAVIEW_COL_RESIZABLE)
        self.columnValue = self.dataViewRules.AppendTextColumn(
            u"Value", wx.dataview.DATAVIEW_CELL_INERT, -1, wx.ALIGN_LEFT, wx.dataview.DATAVIEW_COL_RESIZABLE)
        sizerBoxRule.Add(
            self.dataViewRules,
            1,
            wx.EXPAND | wx.BOTTOM | wx.RIGHT,
            5)

        sizer.Add(sizerBoxRule, 1, wx.EXPAND, 5)

        self.panelActivityControl = wx.Panel(
            self,
            wx.ID_ANY,
            wx.DefaultPosition,
            wx.DefaultSize,
            wx.TAB_TRAVERSAL)
        sizerActivityControl = wx.BoxSizer(wx.HORIZONTAL)

        self.buttonApply = wx.Button(
            self.panelActivityControl,
            wx.ID_ANY,
            u"Apply",
            wx.DefaultPosition,
            wx.DefaultSize,
            0)
        self.buttonApply.SetToolTip(u"Apply activity settings changes.")

        sizerActivityControl.Add(self.buttonApply, 0, wx.ALIGN_RIGHT, 5)

        self.buttonDiscard = wx.Button(
            self.panelActivityControl,
            wx.ID_ANY,
            u"Discard",
            wx.DefaultPosition,
            wx.DefaultSize,
            0)
        self.buttonDiscard.SetToolTip(u"Discard activity settings changes.")

        sizerActivityControl.Add(self.buttonDiscard, 0, 0, 5)

        self.panelActivityControl.SetSizer(sizerActivityControl)
        self.panelActivityControl.Layout()
        sizerActivityControl.Fit(self.panelActivityControl)
        sizer.Add(self.panelActivityControl, 0, wx.ALIGN_RIGHT | wx.TOP, 5)

        self.SetSizer(sizer)
        self.Layout()
        sizer.Fit(self)
