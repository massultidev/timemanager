#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import wx
from gdev_gui_wx.ViewModelBase import ViewModelBase
from time_manager.gui.panel.PanelResultModel import PanelResultModel
from time_manager.gui.panel.PanelResultView import PanelResultView


class PanelResultViewModel(ViewModelBase):

    def __init__(self, parent: wx.Window, model: PanelResultModel):
        super().__init__(
            parent=parent,
            model=model
        )

    def _createView(self, parent: wx.Window) -> wx.Window:
        return PanelResultView(parent=parent)

    def _loadModel(self) -> None:
        result = self.model.result

        self.view.ctrlDataViewListCtrl.DeleteAllItems()
        for decision in result.periodSpendDecisions:
            item = (decision.activityName, str(decision.spentPeriods))
            self.view.ctrlDataViewListCtrl.AppendItem(item)

        self.view.labelSparePeriodCount.SetLabelText(str(result.sparePeriods))
        self.view.labelTotalPeriodCount.SetLabelText(str(self.model.periodSettings.periodCount))
