#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import wx
import wx.dataview


class PanelResultView(wx.Panel):

    def __init__(self, parent):
        super().__init__(parent=parent)

        self.SetMinSize(wx.Size(180, -1))

        sizer = wx.BoxSizer(wx.VERTICAL)

        self.ctrlDataViewListCtrl = wx.dataview.DataViewListCtrl(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0)
        self.columnActivityName = self.ctrlDataViewListCtrl.AppendTextColumn(u"Activity", wx.dataview.DATAVIEW_CELL_INERT, -1,
                                                                             wx.ALIGN_LEFT, wx.dataview.DATAVIEW_COL_RESIZABLE)
        self.columnPeriods = self.ctrlDataViewListCtrl.AppendTextColumn(u"Periods", wx.dataview.DATAVIEW_CELL_INERT, -1,
                                                                        wx.ALIGN_LEFT, wx.dataview.DATAVIEW_COL_RESIZABLE)
        sizer.Add(self.ctrlDataViewListCtrl, 1, wx.EXPAND | wx.BOTTOM, 5)

        sizerCtrl = wx.BoxSizer(wx.HORIZONTAL)

        self.labelSparePeriodText = wx.StaticText(self, wx.ID_ANY, u"Spare periods:", wx.DefaultPosition,
                                                  wx.DefaultSize, 0)
        self.labelSparePeriodText.Wrap(-1)

        self.labelSparePeriodText.SetFont(
            wx.Font(wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD,
                    False, wx.EmptyString))

        sizerCtrl.Add(self.labelSparePeriodText, 1, wx.LEFT, 5)

        self.labelSparePeriodCount = wx.StaticText(self, wx.ID_ANY, u"0", wx.DefaultPosition, wx.DefaultSize, 0)
        self.labelSparePeriodCount.Wrap(-1)

        self.labelSparePeriodCount.SetFont(
            wx.Font(wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL,
                    False, wx.EmptyString))

        sizerCtrl.Add(self.labelSparePeriodCount, 0, wx.ALIGN_CENTER | wx.LEFT, 5)

        self.labelPeriodCountDivider = wx.StaticText(self, wx.ID_ANY, u"/", wx.DefaultPosition, wx.DefaultSize, 0)
        self.labelPeriodCountDivider.Wrap(-1)

        self.labelPeriodCountDivider.SetFont(
            wx.Font(wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL,
                    False, wx.EmptyString))

        sizerCtrl.Add(self.labelPeriodCountDivider, 0, wx.ALIGN_CENTER, 5)

        self.labelTotalPeriodCount = wx.StaticText(self, wx.ID_ANY, u"N", wx.DefaultPosition, wx.DefaultSize, 0)
        self.labelTotalPeriodCount.Wrap(-1)

        self.labelTotalPeriodCount.SetFont(
            wx.Font(wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL,
                    False, wx.EmptyString))

        sizerCtrl.Add(self.labelTotalPeriodCount, 0, wx.ALIGN_CENTER | wx.RIGHT, 5)

        sizer.Add(sizerCtrl, 0, wx.EXPAND, 5)

        self.SetSizer(sizer)
        self.Layout()
