#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import wx
from typing import *
from gdev_gui_wx.ViewModelBase import ViewModelBase
from time_manager.StateEvents import ActivityStateEvent, PeriodSpendRuleStateEvent
from time_manager.ViewStates import ActivityViewState, PeriodSpendRuleViewState
from time_manager.gui.panel.PanelActivityModel import PanelActivityModel
from time_manager.gui.panel.PanelActivityView import PanelActivityView
from time_manager.gui.window.DialogPeriodSpendRuleModel import DialogPeriodSpendRuleModel
from time_manager.gui.window.DialogPeriodSpendRuleViewModel import DialogPeriodSpendRuleViewModel


class PanelActivityViewModel(ViewModelBase):

    def __init__(self, parent: wx.Window, model: PanelActivityModel):
        super().__init__(
            parent=parent,
            model=model
        )
        self._enable(enable=False)

        periodSpendRuleModel = DialogPeriodSpendRuleModel(
            resourceProvider=self.model.resourceProvider,
            stateObserver=self.model.stateObserver
        )
        self._dlgPeriodSpendRule = DialogPeriodSpendRuleViewModel(
            parent=self.view,
            model=periodSpendRuleModel
        )

        activityStatesToSubscribe = [
            ActivityViewState.Display,
            ActivityViewState.Add,
            ActivityViewState.Edit,
            ActivityViewState.Apply,
            ActivityViewState.Dismiss,
        ]
        self._activityViewStateSubscription = self.model.stateObserver.subscribeActivityStates(
            states=activityStatesToSubscribe,
            callback=self._onActivityViewStateChange
        )

        periodSpendRuleStatesToSubscribe = [
            PeriodSpendRuleViewState.Apply,
            PeriodSpendRuleViewState.Dismiss
        ]
        self._periodSpendRuleViewStateSubscription = self.model.stateObserver.subscribePeriodSpendRuleStates(
            states=periodSpendRuleStatesToSubscribe,
            callback=self._onPeriodSpendRuleViewStateChange
        )

    def _createView(self, parent: wx.Window) -> wx.Window:
        view = PanelActivityView(
            parent=parent
        )

        view.buttonAddRule.Bind(wx.EVT_BUTTON, self._onRuleAdd)
        view.buttonEditRule.Bind(wx.EVT_BUTTON, self._onRuleEdit)
        view.buttonDeleteRule.Bind(wx.EVT_BUTTON, self._onRuleDelete)
        view.buttonDeleteAllRules.Bind(wx.EVT_BUTTON, self._onRuleDeleteAll)
        view.buttonApply.Bind(wx.EVT_BUTTON, self._onApply)
        view.buttonDiscard.Bind(wx.EVT_BUTTON, self._onDiscard)

        return view

    def _loadModel(self) -> None:
        activity = self.model.activity

        self.view.textFieldName.SetValue(activity.name)
        self.view.checkBoxIsRequired.SetValue(activity.isRequired)

        self.view.dataViewRules.DeleteAllItems()
        for rule in activity.periodSpendRules.values():
            item = (str(rule.periods), str(rule.valueToGain))
            self.view.dataViewRules.AppendItem(item)

    def _dumpModel(self) -> None:
        activity = self.model.activity
        activity.name = self.view.textFieldName.GetValue()
        activity.isRequired = self.view.checkBoxIsRequired.GetValue()

    def _enable(self, enable: Optional[bool] = True) -> None:
        self.view.Enable(enable)

    def _onActivityViewStateChange(self, event: ActivityStateEvent) -> None:
        eventId = event.eventId

        if eventId in [ActivityViewState.Display, ActivityViewState.Add, ActivityViewState.Edit]:
            self.model.reset()

            if eventId in [ActivityViewState.Display, ActivityViewState.Edit]:
                self.model.setEditedActivity(event.activityName)

            self.refresh()

            if eventId in [ActivityViewState.Add, ActivityViewState.Edit]:
                self._enable(enable=True)

            return

        if eventId in [ActivityViewState.Apply, ActivityViewState.Dismiss]:
            self._enable(enable=False)

    def _onPeriodSpendRuleViewStateChange(self, event: PeriodSpendRuleStateEvent) -> None:
        if event.eventId == PeriodSpendRuleViewState.Apply:
            self.refresh()

    def _onRuleAdd(self, event: wx.Event) -> None:
        self._dumpModel()
        self._updatePeriodSpendRuleDialogModel()

        self.model.stateObserver.notifyPeriodSpendRuleAdd()

    def _onRuleEdit(self, event: wx.Event) -> None:
        self._dumpModel()
        self._updatePeriodSpendRuleDialogModel()

        periods = self._getSelectedPeriods()
        if periods is not None:
            self.model.stateObserver.notifyPeriodSpendRuleEdit(periods)

    def _onRuleDelete(self, event: wx.Event) -> None:
        periods = self._getSelectedPeriods()
        if periods is not None:
            self.model.activity.periodSpendRules.remove(periods)
            self.refresh()

    def _onRuleDeleteAll(self, event: wx.Event) -> None:
        rules = self.model.activity.periodSpendRules
        if len(rules) == 0:
            return

        answer = wx.MessageBox(
            caption='Confirmation',
            message='Are you sure you want to clear period spend rule list?',
            style=wx.OK | wx.CANCEL | wx.CANCEL_DEFAULT | wx.ICON_QUESTION | wx.CENTER
        )
        if answer == wx.CANCEL:
            return

        rules.clear()
        self.refresh()

    def _onApply(self, event: wx.Event) -> None:
        self._dumpModel()

        try:
            self.model.save()
        except RuntimeError as e:
            wx.MessageBox(
                caption='Error',
                message=str(e),
                style=wx.OK | wx.ICON_ERROR | wx.CENTER
            )
            return

        self.model.stateObserver.notifyActivityApply(self.model.activity.name)

    def _onDiscard(self, event: wx.Event) -> None:
        self.model.reset()
        self.refresh()
        self.model.stateObserver.notifyActivityDismiss()

    def _updatePeriodSpendRuleDialogModel(self) -> None:
        model = self._dlgPeriodSpendRule.model
        model.rules = self.model.activity.periodSpendRules

    def _getSelectedPeriods(self) -> Optional[int]:
        selectedRow = self.view.dataViewRules.GetSelectedRow()
        if selectedRow == wx.NOT_FOUND:
            return None

        periodsStr = self.view.dataViewRules.GetTextValue(row=selectedRow, col=0)

        return int(periodsStr)
