#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from gdev_utility.ResourceProvider import ResourceProvider


class AppInfo:

    def __init__(self, resourceProvider: ResourceProvider):
        self._resourceProvider = resourceProvider

    @property
    def name(self) -> str:
        return 'TimeManager'

    @property
    def version(self) -> str:
        versionFile = self._resourceProvider.getResourcePath('version.txt')
        with open(versionFile) as f:
            return 'v.{}'.format(f.read().strip())
