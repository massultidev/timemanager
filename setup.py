#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import os
import sys
from setuptools import setup
from setuptools import find_packages


def assert_working_dir():
    root_dir = os.path.dirname(os.path.realpath(__file__))
    if os.getcwd() != root_dir:
        sys.stderr.write("[ERROR]: Please change working directory to: {}\n".format(root_dir))
        sys.exit(1)


def get_requirements():
    with open('requirements.cfg') as f:
        return f.read().splitlines()


def get_version():
    with open('pkg/time_manager/_resource/version.txt') as f:
        return f.read().strip()


assert_working_dir()

setup(
    name='time_manager',
    version=get_version(),
    author='P.J. Grochowski',
    package_dir={'': 'pkg'},
    packages=find_packages('pkg'),
    include_package_data=True,
    zip_safe=False,
    install_requires=get_requirements(),
    scripts=['script/TimeManager']
)
