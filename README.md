|![](/readme-logo.png)
|:-:
| Helps you make your mind.

# Description

The perfect application for people that have lots of activities but limited time to spend.

TimeManager supports decision making on time spend per activity.  
Only thing that you need to know is how much either of those activities are important to you.  
Like in a sense of points. Don't worry, it's easy, the application will guide you.  

So if you just cannot decide, use TimeManager!

# Install

There are more than one ways you can acquire this application.  
However for most users the simplest way should be enough.

### Basic

This method of installation comes basically to downloading and extracting 7zip archive.
In effect you acquire exexcutable file.
Which you can run without aby external dependencies.
Also store anywhere you what.

##### To download standalone executable
- Go to [Downloads](https://bitbucket.org/massultidev/timemanager/downloads/) section.
- Download package for your platform.
- Make sure you have 7zip installed on your system.
- Extract downloaded 7zip archive.
- Now you have the application executable.
- Run it the usual way.

### Advanced

Make sure your Python version is at least: 3.6

##### To install the application as Python package
- Install packages: pip, setuptools
- Install dependencies not available from upstream: [gdevpy_utility](https://bitbucket.org/massultidev/gdevpy_utility/src/master/), [gdevpy_guiwx](https://bitbucket.org/massultidev/gdevpy_guiwx/src/master/)
- Install dependencies: pip install -r requirements.cfg
- Build package: python setup.py build
- Install package: python setup.py install
- Run application: TimeManager

##### To build standalone executable yourself:
- First you need to perform steps from previous section.
- Install dependencies: pip install -r requirements_bin.cfg
- Find standalone executable in: dist_bin/
